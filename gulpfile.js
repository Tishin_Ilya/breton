var gulp = require('gulp');
var browserSync = require('browser-sync');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var uncss = require('gulp-uncss');
var csso = require('gulp-csso');
var svgmin = require('gulp-svgmin');
var imagemin = require('gulp-imagemin');
var htmlhint = require("gulp-htmlhint");
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var sourcemaps = require('gulp-sourcemaps');
var reload = browserSync.reload;
var rimraf = require('rimraf');
var concat = require('gulp-concat');
var jade = require('gulp-jade');
var pug = require('gulp-pug');
var pngquant = require('imagemin-pngquant');
var rename = require('gulp-rename');
var mainBowerFiles = require('main-bower-files');
var filter = require('gulp-filter');

var path = {
    build: {
        html: 'websitestructure/',
        js: {
          custom: 'websitestructure/commons/js/',
          vendor: 'websitestructure/commons/js/vendor/'
        },
        css: {
          custom: 'websitestructure/commons/css/',
          vendor: 'websitestructure/commons/css/vendor/'
        },
        img: 'websitestructure/commons/images/',
        fonts: 'websitestructure/commons/fonts/',
        videos: 'websitestructure/commons/video/'
    },
    src: {
        html: 'src/*.html',
        pug: 'src/pug/pages/*.pug',
        js: {
          custom: 'src/js/*.js',
          vendor: 'src/js/vendor/**/*.js'
        },
        sass: 'src/scss/*.scss',
        img: 'src/images/**/*',
        fonts: 'src/fonts/**/*',
        videos: 'src/video/**/*.*'
    },
    watch: {
        pug: 'src/**/*.pug',
        js: 'src/js/**/*.js',
        sass: 'src/scss/**/*.scss',
        img: 'src/images/**/*',
        fonts: 'src/fonts/**/*',
        libs: 'bower_components/**/*'
    },
    clean: './websitestructure'
};

// HTML-BUILD
gulp.task('pug:build', function(err) {
  if (err) {
    console.log(err);
  }

  return gulp.src(path.src.pug)
      .pipe(pug({
                  pretty: true
              }))
      .pipe(gulp.dest(path.build.html))
      .pipe(reload({stream: true}));
});

// BOWER_BUILD
gulp.task('libs', function () {
  // JS
  var jsFilter = filter('src/libs/**/*.js');

  gulp.src(mainBowerFiles())
      .pipe(jsFilter)
      .pipe(gulp.dest(path.build.js.vendor));

  // CSS
  var cssFilter = filter(['src/libs/**/*.css', '!src/libs/**/*.min.css']);
  var cssMinFilter = filter('src/libs/**/*.min.css');

  gulp.src(mainBowerFiles())
      .pipe(cssFilter)
      .pipe(csso())
      .pipe(rename({
        extname: ".min.css"
      }))
      .pipe(gulp.dest(path.build.css.vendor));
  gulp.src(mainBowerFiles())
      .pipe(cssMinFilter)
      .pipe(gulp.dest(path.build.css.vendor));

  // Fonts
  var fontsFilter = filter([
      'src/libs/**/*.otf',
      'src/libs/**/*.eot',
      'src/libs/**/*.ttf',
      'src/libs/**/*.svg',
      'src/libs/**/*.woff',
      'src/libs/**/*.woff2'
  ]);

  gulp.src(mainBowerFiles())
      .pipe(fontsFilter)
      .pipe(gulp.dest(path.build.fonts));

  // Images
  var imagesFilter = filter([
    'src/libs/**/*.jpg',
    'src/libs/**/*.jpeg',
    'src/libs/**/*.png',
    'src/libs/**/*.gif',
    'src/libs/**/*.svg'
  ]);

  gulp.src(mainBowerFiles())
  .pipe(imagesFilter)
  .pipe(gulp.dest(path.build.css.vendor));
});

// JS_BUILD
gulp.task('js:custom', function () {
  gulp.src([path.src.js.custom, '!src/js/workshop-map.js', '!src/js/maps.js'])
    .pipe(sourcemaps.init())
    .pipe(concat('main.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.js.custom))
    .pipe(reload({stream: true}));

  gulp.src(['src/js/workshop-map.js', 'src/js/maps.js'])
      .pipe(gulp.dest(path.build.js.custom));
});

gulp.task('js:build', ['js:custom']);

// STYLES-BUILD
gulp.task('sass:build', function() {
    var supportedBrowsers = [
        '> 0.5%',
        'last 2 versions',
        'ie >= 8',
        'ie_mob >= 10',
        'ff >= 30',
        'chrome >= 34',
        'safari >= 7',
        'opera >= 23',
        'ios >= 7',
        'android >= 4.1',
        'bb >= 10'
    ];
    gulp.src([path.src.sass])
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([
          autoprefixer({
            browsers: supportedBrowsers,
            cascade: false
          })
        ]))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css.custom))
        .pipe(reload({stream: true}));

});

// IMAGE-BUILD
gulp.task('image:build', function () {
    return gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});

// FONTS-BUILD
gulp.task('fonts:build', function() {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
        .pipe(reload({stream: true}));
});

// Copy files
gulp.task('copy', function () {
  // videos
  gulp.src(path.src.videos).pipe(gulp.dest(path.build.videos));
});

// ALL BUILDS
gulp.task('build', [
  'copy',
  'libs',
  'pug:build',
  'js:build',
  'sass:build',
  'fonts:build',
  'image:build'
]);

// WATCH-TASK
gulp.task('watch', function() {
    watch([path.watch.pug], function(event, cb) {
        gulp.start('pug:build');
    });
    watch([path.watch.libs], function(event, cb) {
        gulp.start('libs');
    });
    watch([path.watch.sass], function(event, cb) {
        gulp.start('sass:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});

gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: "./websitestructure"
        },
        tunnel: true,
        host: 'localhost',
        port: 3000
    });
});

gulp.task('clean', function (cb) {
    return rimraf(path.clean, cb);
});

gulp.task('default', ['build', 'serve', 'watch']);
