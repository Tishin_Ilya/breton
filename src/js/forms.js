$(document).ready(function() {
    $('input[type="text"]').blur(toggleActive);
    $('input[type="tel"]').blur(toggleActive);
    $('textarea').blur(toggleActive);
    $('input[type="site"]').blur(toggleActive);
    $('input[type="password"]').blur(toggleActive);
    $('input[type="email"]').blur(toggleActive);
    function toggleActive() {
      if( $(this).val().length >= 1) {
          $(this).addClass('active');
      }
      else {
          $(this).removeClass('active');
      }
    };
});
