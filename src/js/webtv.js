

$(window).load(function() {
    $(".webtv-list__channel").on('click', '.webtv-channel__title', function (e) {
      e.preventDefault();
      $(this).toggleClass("active");
    });
    $(".webtv-carousel__items").on('init', function() {
        $('.webtv-channel__title').first().addClass('active');
    });
    $('.webtv-carousel__items').slick({
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    
    // $("#more-info-display-6").slideDown();
});
