$(window).load(function() {
    var $masonrySections = $('.division-news__content');

    if($masonrySections.length) {
      $masonrySections.masonry({
        itemSelector: '.division-news__item',
        columnWidth: '.division-news__sizer',
        percentPosition: true
      });
    }
});
