$(document).ready(
    function() {
        $(".slider-nav .slick-list").mCustomScrollbar(
            {
                advanced: {
                    updateOnContentResize: true,
                },
                mouseWheel:{
                    enable: false,
                },
                axis: "x",
                scrollButtons: {
                  enable: true
                },
                scrollButtons:{ scrollType: "stepped" }
            }
        );
        $(".slider-division").mCustomScrollbar(
            {
                mouseWheel:{
                    enable: false,
                },
                axis: "x",
                scrollButtons: {
                  enable: true
                },
                theme: "breton"
            }
        );
        $(".jcf-select-drop").mCustomScrollbar(
            {
                advanced: {
                    updateOnContentResize: true,
                },
                mouseWheel:{
                    enable: false,
                },
                axis: "y"
            }
        );
    }
);
$(function() {
    $(window).resize(mCustomScrollbarAaptive);
});
function mCustomScrollbarAaptive() {
  if ($(".slider-nav .slick-list").length || $(".slider-division .slick-list").length || $(".jcf-select-drop").length) {
    $(".slider-nav .slick-list").mCustomScrollbar("update");
    $(".slider-division .slick-list").mCustomScrollbar("update");
    $(".jcf-select-drop").mCustomScrollbar.refresh("update");
  }
}
