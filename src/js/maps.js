/**
*map
*/

var map;
function initMap() {
    var myLatLng = {lat: 45.6515549564207, lng: 11.87268935213724};
    var map = new google.maps.Map(document.getElementById('contact-us-map'), {
        center: myLatLng,
        zoom: 10,
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);
    var image = 'commons/images/marker.png';

    service.getDetails({
        placeId: 'ChIJq7mRcL8peUcRhKyjMM-OTbs'
    }, function(place, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            var marker = new google.maps.Marker({
                position: place.geometry.location,
                map: map,
                icon: image
            });
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent(place.name);
                infowindow.open(map, this);
            });
        }
    });
}
