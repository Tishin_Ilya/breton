/**
*workshop map
*/

var bigmap;
function initMap() {
    var myLatLng = {lat: 15.6515549564207, lng: 11.87268935213724};
    var bigmap = new google.maps.Map(document.getElementById('workshop-map'), {
        center: myLatLng,
        zoom: 2,
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(bigmap);
    var image = 'commons/images/marker.png';


    service.getDetails({
        placeId: 'ChIJq7mRcL8peUcRhKyjMM-OTbs'
    }, function(place, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            var marker = new google.maps.Marker({
                position: place.geometry.location,
                map: bigmap,
                icon: image
            });
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent(place.name);
                infowindow.open(bigmap, this);
            });
        }
    });

    setMarkers(bigmap);
}

var offices = [
    ['Bondi Beach', -33, 151],
    ['Coogee Beach', -13.923036, 51.259052, 5],
    ['Cronulla Beach', -84.028249, 11.157507, 3],
    ['Manly Beach', 23.80010128657071, 65.28747820854187, 2],
    ['Maroubra Beach', 20.950198, 31.259302, 1]
];

function setMarkers(bigmap) {
    // Adds markers to the map.

    // Marker sizes are expressed as a Size of X,Y where the origin of the image
    // (0,0) is located in the top left of the image.

    // Origins, anchor positions and coordinates of the marker increase in the X
    // direction to the right and in the Y direction down.
    var image = {
        url: 'commons/images/marker.png',
        // This marker is 20 pixels wide by 32 pixels high.
    };
    // Shapes define the clickable region of the icon. The type defines an HTML
    // <area> element 'poly' which traces out a polygon as a series of X,Y points.
    // The final coordinate closes the poly by connecting to the first coordinate.
    var shape = {
        coords: [1, 1, 1, 20, 18, 20, 18, 1],
        type: 'poly'
    };
    for (var i = 0, len = offices.length; i < len; i++) {
        var office = offices[i];
        var marker = new google.maps.Marker({
            position: {lat: office[1], lng: office[2]},
            map: bigmap,
            icon: image,
            shape: shape,
            title: office[0],
            zIndex: office[3]
        });
    }
}
