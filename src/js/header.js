
$(document).ready(function() {
    //bottom menu links
    $("#bottom-link-1").click(function () {
        if($(".header-subbotom__container") && !$("#header-subbotom__container-1").hasClass("active")) {
            $(".header-subbotom__container").removeClass("active");
            $("#bottom-link-1").removeClass("active")
        }
        $("#header-subbotom__container-1").toggleClass("active");
        $("#bottom-link-1").toggleClass("open")
    });
    $("#header-menu-hamburger").click(function () {
        $("#header-mobile-menu").toggleClass("active");
    });

    // Toggle blocks
    $('.toggle-block').on('click', '.toggle-btn', function() {
        var $toggleGroup = $(this).closest('.toggle-group');
        var $toggleContent = $(this).closest('.toggle-block').find('.toggle-block__content');

        if ($toggleGroup.length) {
          $toggleGroup.find('.toggle-block__content').not($toggleContent).removeClass('open');
        }

        $toggleContent.toggleClass('open');
    });

    // Hide toggle blocks when click outside
    $(document).on('click', function(event) {
    var $toggleBlocks = $(event.target).closest('.toggle-block');
    var $toggleContent = $('.toggle-block__content');

      if(!$toggleBlocks.length) {
        if($toggleContent.is(":visible")) {
          $toggleContent.removeClass('open');
        }
      } else {
        var $clickedToggleContent = $toggleBlocks.find('.toggle-block__content');

        if($toggleContent.is(":visible")) {
          $toggleContent.not($clickedToggleContent).removeClass('open');
        }
      }
    });

    $("#more-info-switch-1").click(function () {
        $("#more-info-display-1").toggleClass("active");
        $("#more-info-switch-1").toggleClass("active");
        $('.webtv-carousel__items').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 4
        });
    });
    $("#more-info-switch-2").click(function () {
        $("#more-info-display-2").toggleClass("active");
        $("#more-info-switch-2").toggleClass("active");
        $('.webtv-carousel__items').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 4
        });
    });
    $("#more-info-switch-3").click(function () {
        $("#more-info-display-3").toggleClass("active");
        $("#more-info-switch-3").toggleClass("active");
        $('.webtv-carousel__items').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 4
        });
    });
    $("#more-info-switch-4").click(function () {
        $("#more-info-display-4").toggleClass("active");
        $("#more-info-switch-4").toggleClass("active");
    });

});
