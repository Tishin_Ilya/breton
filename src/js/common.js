$(function() {
  /**
   * Animate Counter
   */
  var animateCounterModule = (function() {
    function init() {
      _setUpListeners();
    }
    function _setUpListeners() {
      _startAnimation();
    }

    function _startAnimation() {
      var $numElement = $('.map_number__number');
      $numElement.each(function(i, item) {
        var $item = $(item),
            prefix = $item.data('prefix') || '',
            start = $item.data('start') || 0,
            end = $item.data('end'),
            duration = 1500;

        $item.prop('Counter', start).animate({
          Counter: end
        }, {
          duration: duration,
          easing: 'swing',
          step: function(now) {
            var num = Math.ceil(now).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $item.text(prefix + ' ' + num);
          }
        })
      })
    }
    return {
      init: init
    };
  })();

  if ($('.map__legend').length) {
    var inview = new Waypoint.Inview({
      element: $('.map__legend')[0],
      enter: function() {
        animateCounterModule.init();

        inview.destroy();
      }
    })
  }

  var calculateWidthModule = (function() {
    function init() {
      _calc();
      _setUpListeners();
    }

    function _setUpListeners() {
      $(window).resize(function() {
        _calc();
      });
    }

    function _calc() {
      var wrap = $('.slider-division'),
          wrapWidth = $(wrap).width(),
          slidesCount = $('.slider-division__slide-wrap').length,
          visibleCount = getCount(wrapWidth),
          slideWidth = wrapWidth / visibleCount;
      $('.slider-division__slide-wrap').css('width', slideWidth);
      $('.mCSB_container').css('width', slidesCount * slideWidth);
    }

    function getCount(width) {
      if (width >= 800) return 4;
      if (width < 800 && width >= 600) return 3;
      if (width < 600 && width >= 360) return 2;
      if (width < 360) return 1;
    }

    return {
      init: init
    };
  })();

  calculateWidthModule.init();

  var syncSliderModule = (function() {

    function init() {
      _setUpListeners();
      $('.slider-division__slide-wrap').each(function(i, item) {
        item.setAttribute("data-number", i);
      });
    }

    function _setUpListeners() {
      $('.slider-division').on('click', '.slider-division__slide-wrap', function() {
        var $this = $(this),
            $currentSlide = $this.find('.slider-division__slide'),
            $otherSlides = $this.siblings('.slider-division__slide-wrap').find('.slider-division__slide'),
            $targetSlider = $('.slider-for'),
            slideNum = $this.data('number');
        $targetSlider.slick('slickGoTo', slideNum, false);
        $otherSlides.removeClass('active');
        $currentSlide.addClass('active');
        moveCenter($this);
        _stopVideo();
      })
    };

    function _stopVideo(){
      var $player = $('.youtube');
      $player.each(function(i, item) {
        item.contentWindow.postMessage('{"event":"command", "func":"pauseVideo", "args":""}', '*');
      });
    };

    function moveCenter(el) {
      var left = el.offset().left,
          docWidth = $(document).width(),
          delta = el.width()/2,
          position = docWidth/2 - left - delta;
      $(".slider-division").mCustomScrollbar("scrollTo","+=" + position);
    };

    return {
      init: init
    };
  })();

  syncSliderModule.init();

  var submenuModule = (function() {
    function init() {
      _setUpListeners();
    };
    function _setUpListeners() {
      $('.products').on('click', '.products__link', function(e) {
        e.preventDefault();
        var $this = $(this),
            $other = $this
                .closest('.products__item')
                .siblings('.products__item')
                .find('.products__link');
        if ($this.hasClass('active')) {
          closeMenu($this);
          $this.removeClass('active');
        } else {
          $other.each(function(i, item) {
            closeMenu(item);
            $(item).removeClass('active');
          });
          openMenu($this);
          $this.addClass('active');
        };
      });
      $(window).resize(function() {
        var $link = $('.active'),
            $item = $link.closest('.products__item'),
            $list = $item.find('.products-inner'),
            margin = $item.css('margin-bottom'),
            listHeight = $list.outerHeight();

        if (margin != listHeight) {
          $item.css('margin-bottom', listHeight);
        };
      });
      $('.products-inner__title-link').on('click', function(e) {
        e.preventDefault();
        var $this = $(this),
            $item = $this.closest('.products__item'),
            $link = $item.find('.active'),
            $inner = $item.find('.products-inner');
        $inner.stop(true,true).fadeOut(300,function() {
          $link.removeClass('active');
          $item.css('margin-bottom', 0);
        });
      });
    };

    function closeMenu(el) {
      var $parent = $(el).closest('.products__item'),
          $inner = $parent.find('.products-inner');
      $inner.stop(true,true).fadeOut(300,function() {
        $parent.css('margin-bottom', 0);
      });
    };

    function openMenu(el) {
      var $parent = $(el).closest('.products__item'),
          $inner = $parent.find('.products-inner'),
          height = $inner.outerHeight();
      $parent.css('margin-bottom', height);
      $inner.stop(true,true).fadeIn(400);
    }

    return {
      init: init
    };
  })();

  submenuModule.init();

  var backToTopModule = (function() {
    function init() {
      _setUpListeners();
    };
    function _setUpListeners() {
      $(window).scroll(function(){
        if ($(this).scrollTop() > 800) {
          $('.footer__anchor').fadeIn(300, function() {
            $(this).addClass('visible').removeAttr('style');
          })
        } else {
          $('.footer__anchor').fadeOut(300, function() {
            $(this).removeClass('visible').removeAttr('style');
          })
        }
      });
      $('.footer__anchor').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
          scrollTop:0
        }, 500);
      });
    };
    return {
      init: init
    };
  })();

  backToTopModule.init();

  var submenuNavModule = (function() {
    function init() {
      _setUpListeners();
    };
    function _setUpListeners() {
      $('.header-bottom').on('click', '.header-bottom__link', function(e) {
        e.preventDefault();
        var $this = $(this),
            $menu = $('.header-bottom'),
            $otherLinks = $this.siblings('.header-bottom__link'),
            dataName = $this.data('name'),
            $targetList = $menu.find('.products').filter('[data-name='+ dataName +']'),
            $otherLists = $menu.find('.products').not($targetList);
        if ($this.hasClass('open')) {
          if (!$menu.hasClass('header-bottom--product-list')) {
            $this.removeClass('open');
            $targetList.removeClass('active');
          }
        } else {
          $otherLinks.removeClass('open');
          $this.addClass('open');
          $targetList.addClass('active');
          $otherLists.removeClass('active');
        }
      });
      $(document).on('click', function(e) {
        var $target = $(e.target),
            $menu = $('.header-bottom'),
            $lists = $menu.find('.products'),
            $activeLink = $menu.find('.open');
        if(!$target.closest('.header-bottom').length && $lists.is(":visible") && !$menu.hasClass('header-bottom--product-list')) {
          $lists.removeClass('active');
          $activeLink.removeClass('open');
        };
      });
    };
    return {
      init: init
    };
  })();

  submenuNavModule.init();

  /**
   * Dropdowns
   */
  var dropdownsModule = (function() {
    function init() {
      _setUpListeners();
    };
    function _setUpListeners() {
      $('.header').on('click', '.js-dropdown-link', function(e) {
        e.preventDefault();
        var $this = $(this),
            $dropdown = $this.siblings('.js-dropdown-form'),
            $otherDropdowns = $('.js-dropdown-form').not($dropdown);
        $otherDropdowns.filter('.active').removeClass('active');
        $dropdown.toggleClass('active');
      });
      $(document).on('click', function(e) {
        var $target = $(e.target),
            $dropdowns = $('.js-dropdown-form').filter('.active');
        if(!$target.closest($dropdowns).length && $dropdowns.is(":visible") && !$target.hasClass('js-dropdown-link')) {
          $dropdowns.removeClass('active');
        };
      });

    };
    return {
      init: init
    };
  })();

  dropdownsModule.init();

  /**
   * Custom forms
   */
  jcf.replaceAll();

  /**
   * Popups
   */
  $('.open-popup--iframe').magnificPopup({
    type: 'iframe',
    iframe: {
      patterns: {
        youtube: {
          index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

          id: 'v=', // String that splits URL in a two parts, second part should be %id%
          // Or null - full URL will be returned
          // Or a function that should return %id%, for example:
          // id: function(url) { return 'parsed id'; }

          src: 'h%id%&autoplay=1' // URL that will be set as a source for iframe.
        }
      }
    }
  });

  $('.open-popup--inline').magnificPopup({
    type: 'inline'
  });

  $('.open-popup--image').magnificPopup({
    type: 'image'
  });

  $('.popup-gallery').magnificPopup({
    type: 'iframe',
    delegate: 'a',
    gallery: {
      enabled: true
    },
    iframe: {
      patterns: {
        youtube: {
          index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

          id: 'v=', // String that splits URL in a two parts, second part should be %id%
          // Or null - full URL will be returned
          // Or a function that should return %id%, for example:
          // id: function(url) { return 'parsed id'; }

          src: 'h%id%&autoplay=1' // URL that will be set as a source for iframe.
        }
      }
    }
  });
});
