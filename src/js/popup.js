function showPopup() {
  $(".popup-reserved-area").fadeIn(0);
}
function hiddenPopup() {
  $(".popup-reserved-area").fadeOut(0);
}
function showCreatePopup() {
  $(".popup_create-account").fadeIn(0);
}
function hiddenCreatePopup() {
  $(".popup_create-account").fadeOut(0);
}

$(document).ready(function() {
    $(".popup__background").click(function(){
        $(".popup-reserved-area").fadeOut(0);
        $(".popup_create-account").fadeOut(0);
    });
});

$(document).ready(function() {
    $("#hiddenCookiePopup").click(function () {
        $(".cookie").addClass("closed");
    });
});

$('.login__content').on('click', '.login__user-contract', showPopup);
