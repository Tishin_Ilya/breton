$(document).ready(function(){
  function absoluteCover($elements) {
    $elements.each(function() {
      // set size
      var $element = $(this); // element

      $element.removeClass('fullHeight fullWidth');

      var $elementContainer = $element.parent(); // element container
      var containerHeight = $elementContainer.height(); // container height
      var containerWidth = $elementContainer.width(); // container width
      var elementHeight = $element.height(); // initial element height
      var elementWidth = $element.width(); // initial element width
      var containerAspectRatio = containerWidth / containerHeight;
      var elementAspectRatio = elementWidth / elementHeight;

      if (containerAspectRatio > elementAspectRatio) { // if portrait
        $element.addClass('fullWidth').removeClass('fullHeight'); // set width 100%
      } else { // if landscape
        $element.addClass('fullHeight').removeClass('fullWidth'); // set height 100%
      }
    });
  }

  var $slickSliderFor = $('.slider-for');

  $slickSliderFor.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true
  });

  $slickSliderFor.on('init setPosition', function() {
    absoluteCover($(this).find('.img'));
  });

  $('.hero-slider').slick({
      focusOnSelect: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: false,
      arrows: true,
      dots: true,
      zIndex: 1,
      responsive: [
        {
          breakpoint: 1025,
          settings: {
            adaptiveHeight: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            arrows: false
          }
        }
      ]
  });
});
