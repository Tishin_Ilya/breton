$(function() {
  var $contentTables = $('.table');
  var $thtr = $contentTables.find('.thead .tr');
  var $th = $thtr.find('.th');
  var $trs = $contentTables.find('.tr').not($thtr);

  for (var i = 0; i <= $trs.length; i++) {
    var $tds = $trs.eq(i).find('.td');

    for (var j = 0; j <= $tds.length; j++) {
      $th.eq(j).clone().insertBefore($tds.eq(j)).addClass('desktop-hidden');
    }
  }
});
