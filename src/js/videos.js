/**
 * Youtube API
 */
//  This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//  This function creates an <iframe> (and YouTube player)
//  after the API code downloads.
var playersArray = {};

function onYouTubeIframeAPIReady() {
  $(function() {
    var player;
    var $players = $('iframe[src*="youtube.com"]');

    $players.each(function( index ) {
      $(this).data('youtube-api', index);
      $(this).attr('id', 'youtube-video-' + index);
      player = new YT.Player('youtube-video-' + index, {
        events: {
          'onStateChange': onPlayerStateChange
        }
      });
      playersArray['player-' + index] = player;
    });
  });
}

function onPlayerStateChange(e) {
  if (e.data === 1) {
    stopOtherVideos($(e.target.a))
  }
}

function stopOtherVideos($activeVideos) {
  var $otherVideos = $('.video').not($activeVideos);

  $otherVideos.each(function(index) {
    if(this.tagName === 'IFRAME') {
      var player = playersArray['player-' + $(this).data('youtube-api')];
      var playerState = player.getPlayerState();

      if(playerState === 1) {
        player.pauseVideo();
      }
    } else if(this.tagName === 'VIDEO') {
      this.pause();
    }
  });
}

function playVideo($activeVideo) {
  if($activeVideo.prop("tagName") === 'IFRAME') {
    var player = playersArray['player-' + $activeVideo.data('youtube-api')];

    player.playVideo();
  }
  else if($activeVideo.prop("tagName") === 'VIDEO') {
    $activeVideo[0].play();
  }
}

$(window).on('load', function() {
  var $slickSliders = $('.slick-slider');

  $slickSliders.on('beforeChange', function(event, slick, direction) {
    stopOtherVideos();
  });

  $slickSliders.on('afterChange', function(event, slick, direction) {
    playVideo(slick.$slides.filter('.slick-active').find('.video'));
  });
});

$('.open-popup').on('mfpBeforeOpen', function() {
  stopOtherVideos();
});


